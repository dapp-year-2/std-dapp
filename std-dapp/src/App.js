import React, {Component} from 'react';
import Web3 from 'web3';
import './App.css';
import Form from './components/studentForms';
import SubjectList from './components/subjectList';
import MarksList from './components/marklist';
import FindMarks from './components/FindMarks'



import { STUDENT_LIST_ABI, STUDENT_LIST_ADDRESS } from './abi/config_studentList';
import { SUBJECT_LIST_ABI, SUBJECT_LIST_ADDRESS } from './abi/config_subjectlist';
import {MARK_LIST_ABI, MARK_LIST_ADDRESS}  from './abi/config_marklist';

class App extends Component{
  
  componentDidMount(){
    if(!window.ethereum)
      throw new Error("No crypto wallet found. please install it.");
    window.ethereum.send("eth_requestAccounts");
    this.loadBlockchainData()
  }
  async loadBlockchainData(){
    // const web3 = new Web3(Web3.givenProvider || "http://localhost:7545")
    const web3 = new Web3(Web3.givenProvider ||
      'https://sepolia.infura.io/v3/[3f23987157144c08a2950e56f14f7178]'
      )
      
    const accounts = await web3.eth.getAccounts()
    this.setState({account:accounts[0]})
    const studentList = new web3.eth.Contract(
      STUDENT_LIST_ABI, STUDENT_LIST_ADDRESS)
      const subjectList = new web3.eth.Contract(SUBJECT_LIST_ABI,
        SUBJECT_LIST_ADDRESS)
      const marksList = new web3.eth.Contract(MARK_LIST_ABI,
        MARK_LIST_ADDRESS)
        
    this.setState({studentList})
    this.setState({ subjectList })
    this.setState({ marksList })


    const studentCount = await studentList.methods.studentsCount().call()
    this.setState({studentCount})
    const subjectCount = await subjectList.methods.subjectsCount().call()
    const marksCount = await marksList.methods.marksCount().call()
this.setState({ studentCount })
this.setState({ subjectCount })
this.setState({ marksCount })


    // eslint-disable-next-line react/no-direct-mutation-state
    this.state.students = []
    for (var i = 1; i <= studentCount; i++) {
      const student = await studentList.methods.students(i).call();
      this.setState({
        students: [...this.state.students, student]
      })
      
    }
  

// eslint-disable-next-line react/no-direct-mutation-state
this.state.subjects = []
  for (i = 1; i <= subjectCount; i++) {
  const subject = await subjectList.methods.subjects(i).call()
  this.setState({
  subjects: [...this.state.subjects, subject]
  })
}
  }

  
  constructor(props){
    super(props)
    this.state = {
      account: '',
      studentCount: 0,
      students: [],
      subjectCount: 0,
      subjects: [],
      marksCount: 0,
      marks: [],
      loading:true,
      update:false,
      studentObj:{},
      updateCID:0    }
    this.createStudent = this.createStudent.bind(this)
    this.markGraduated = this.markGraduated.bind(this)
    this.createSubject = this.createSubject.bind(this)
    this.markRetired = this.markRetired.bind(this)
    this.createMarks = this.createMarks.bind(this)
    this.findMarks = this.findMarks.bind(this)



  }
  createStudent(cid,name){
    this.setState({loading:true})
    this.state.studentList.methods.createStudent(cid,name)
    .send({from:this.state.account})
    .once('receipt',(receipt)=>{
      this.setState({loading:false})
      this.loadBlockchainData()
    })
  }
  markGraduated(cid) {
    this.setState({ loading: true })
    this.state.studentList.methods.markGraduated(cid)
    .send({ from: this.state.account })
    .once('receipt', (receipt) => {
    this.setState({ loading: false })
    this.loadBlockchainData()
    })
    }

    createSubject(id, name) {
      this.setState({ loading: true })
      this.state.subjectList.methods.createSubject(id, name)
      .send({ from: this.state.account })
      .once('receipt', (receipt) => {
      this.setState({ loading: false })
      this.loadBlockchainData()
      })
      }
      markRetired(id) {
      this.setState({ loading: true })
      this.state.subjectList.methods.markRetired(id)
      .send({ from: this.state.account })
      .once('receipt', (receipt) => {
      this.setState({ loading: false })
      this.loadBlockchainData()
      })
      }
    
      createMarks(studentid, subjectid, grades) {
        this.setState({ loading: true })
this.state.marksList.methods.addMarks(studentid, subjectid,
grades)
.send({ from: this.state.account })
.once('receipt', (receipt) => {
this.setState({ loading: false })
this.loadBlockchainData()
})
}
findMarks(studentid,subjectid) {
  this.setState({ loading: true })
  return this.state.marksList.methods
  .findMarks(studentid,subjectid)
  .call({ from: this.state.account })
  }
  


  
  render() {
    return(
      <div className='container'>
        <h1>Students Marks Management System</h1>
        <Form createStudent={this.createStudent.bind(this)} updateName={this.updateName} update ={this.state.update} studentObj = {this.studentObj} />
        <p>Your account: {this.state.account}</p>
        <ul id='studentlsit' className='list-unstyled'>
          {
            this.state.students.map((student, key) => {
              return(
                <li className='list-group-item checkbox' key={key}>
                  <span className='name alert'> {student._id} {student.cid} {student.name} </span>
                  <input className='form-check-input' type='checkbox' name={student._id} defaultChecked ={student.graduated} 
                  disabled={student.graduated} ref={(input) => {
                    this.checkbox = input
                  }}
                  onClick={(event) =>{
                    this.markGraduated(event.currentTarget.name)
                    }} />
                  <label className='form-check-label'>Graduated</label>
                  <button type="submit" className="btn btn-primary" style={{ marginTop: '10px' }} onClick={()=>{this.setState({update:true})
                  
                  this.studentObj= student
                  }} >update</button>
                  
                </li>

              )
            })
          }  
                  <p>Total student : {this.state.studentCount}</p>

        </ul>  
        <SubjectList
createSubject={this.createSubject} />
<p />
<ul id="subjectList" className="list-unstyled">
{

this.state.subjects.map((subject, key) => {
return (
<li className="list-group-item checkbox"
key={key}>
<span className="name alert">{subject._id}.
{subject.code} {subject.name}</span>
<input
className="form-check-input"
type="checkbox"
name={subject._id}
defaultChecked={subject.retired}
disabled={subject.retired}
ref={(input) => {
this.checkbox = input
}}
onClick={(event) => {
this.markRetired(event.currentTarget.name)
}} />
<label className="form-check-label"
>Retired</label>
</li>
)
}
)
}
</ul>
<MarksList
subjects={this.state.subjects}
students={this.state.students}
createMarks={this.createMarks}
/>
<p>Total marks records : {this.state.marksCount}</p>
<FindMarks
subjects = {this.state.subjects}
students = {this.state.students}
findMarks={this.findMarks}
/>


      </div>
    )
  }
}


export default App;




