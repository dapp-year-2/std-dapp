import React, { Component } from 'react'
class MarksList extends Component {
render() {
return (
<form className="p-5 border" onSubmit={(event) => {
event.preventDefault()
this.props.createMarks(
this.studentid.value,
this.subjectid.value,
this.grades.value)
}}>
<h2>Add Marks</h2>
<select id='studentid' className="form-select mb-2"
ref={(input) => { this.studentid = input; }}>
<option defaultValue={true}>Open this select menu</option>
{
this.props.students.map((student, key) => {
return (
<option value={student.cid}>{student.cid}
{student.name}</option>
)
})
}
</select>
<select id='subjectid' className="form-select mb-2"
ref={(input) => { this.subjectid = input; }}>
<option defaultValue={true}>Open this select menu</option>
{
this.props.subjects.map((subject, key) => {
return (
<option value={subject.code}>{subject.code}
{subject.name}</option>
)
})
}
</select>
<select id="grades" className="form-select mb-2"
ref={(input) => { this.grades = input; }}>
<option defaultValue={true}>Open this select menu</option>
<option value="1">A</option>
<option value="2">B</option>
<option value="3">C</option>
<option value="4">D</option>
<option value="5">F</option>
</select>
<input className="form-control btn btn-primary" type="submit"
hidden="" />
</form>
);
}
}
export default MarksList;