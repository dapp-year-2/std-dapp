import React, { useState } from 'react';

function Form(props) {
  const [formData, setFormData] = useState({
    cid: '',
    name: '',
  });

  const handleChange = (event) => {
    setFormData({
      ...formData,
      [event.target.name]: event.target.value,
    });
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    const {cid,name} = formData
    if(cid==="" || name ===""){
      alert("cid and name cannot be empty")
    }
    props.createStudent(cid,name)
    setFormData({
      cid: '',
      name: '',
    })
   
  };
  const handleUpdateName =(e)=>{
    e.preventDefault()
    props.updateName(props.studentObj._id,formData.name)
    setFormData({
      cid: '',
      name: '',
    })
    
  }

  return (
    <form  style={{ border: '1px solid black', padding: '20px', margin: '20px auto', width: '50%', textAlign: 'center' }}>
      {props.update ? <div>
        <div className="form-group">
          <label htmlFor="cid">CID</label>
          <input
            type="text"
            className="form-control"
            id="cid"
            name="cid"
            value={props.studentObj.cid}

          />
        </div>
        <div className="form-group">
          <label htmlFor="name">Name</label>
          <input
            type="text"
            className="form-control"
            id="name"
            name="name"
            value={formData.name}
            onChange={handleChange}
          />
        </div>
        <button onClick={(e)=>handleUpdateName(e)} type="submit" className="btn btn-primary" style={{ marginTop: '10px' }} >
          update
        </button>
        
      </div> :<div>
        <div className="form-group">
          <label htmlFor="cid">CID</label>
          <input
            type="text"
            className="form-control"
            id="cid"
            name="cid"
            value={formData.cid}
            onChange={handleChange}
          />
        </div>
        <div className="form-group">
          <label htmlFor="name">Name</label>
          <input
            type="text"
            className="form-control"
            id="name"
            name="name"
            value={formData.name}
            onChange={handleChange}
          />
        </div>
        <button onClick={(e)=>handleSubmit(e)} type="submit" className="btn btn-primary" style={{ marginTop: '10px' }}>
          submit
        </button>

      </div>}
      
      
      
      
      
    </form>
  );
}

export default Form;