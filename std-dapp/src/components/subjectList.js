import React, { Component } from 'react'
class SubjectList extends Component {
render() {
return (
<form className="p-5 border" onSubmit={(event) => {
event.preventDefault()
this.props.createSubject(this.cid.value,
this.subject.value)
}}>
<h2>Add Subject</h2>
<input id="newCode" type="text"
ref={(input)=>{this.cid = input;}}
className="form-control m-1"
placeholder="Subject Code"
required
/>
<input id="newSubject" type="text"
ref={(input)=>{this.subject = input;}}
className="form-control m-1"
placeholder="Subject Name"
required
/>
<input className="form-control btn btn-primary"
type="submit" hidden="" />
</form>
);
}
}
export default SubjectList;