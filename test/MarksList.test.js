//Import the MarksList smart contract
const MarksList = artifacts.require('MarksList')
const StudentList = artifacts.require('StudentList')
const SubjectList = artifacts.require('SubjectList')
//Use the contract to write all tests
//variable: account => all accounts in blockchain
contract('MarksList', (accounts) => {
//Make sure contract is deployed and before
//we retrieve the all dependent object for testing
beforeEach(async () => {
this.marksList = await MarksList.deployed()
this.studentList = await StudentList.deployed()
this.subjectList = await SubjectList.deployed()
})
//Testing the deployed student contract
it('deploys successfully', async () => {
//Get the address which the student object is stored
const address = await this.marksList.address
//Test for valid address
isValidAddress(address)
})
//Testing the content in the contract
it('adding test students', async () => {
return StudentList.deployed().then(async(instance) => {
s = instance;
studentCid = 2;
return s.createStudent(studentCid++, "Tshering").then(async(tx)=>{
    return s.createStudent(studentCid++, "Dorji Tshering").then(async(tx)=>{
        return s.createStudent(studentCid++, "Chimi Dema").then(async(tx)=>{
        return s.createStudent(studentCid++, "Tashi Phuntsho").then(async(tx)=>{
        return s.createStudent(studentCid++, "Samten Zangmo").then(async(tx)=>{
        return s.createStudent(studentCid++, "Chimi Dema").then(async(tx)=>{
        return s.studentsCount().then(async(count)=>{
        assert.equal(count,6)
        })
        })
        })
        })
        })
        })
        })
        })
        })
        it('test adding subjects', async () => {
        return SubjectList.deployed().then((instance) => {
        s = instance;
        return s.createSubject("CSF102", "Fundamentals of Computing").then(()=>{
        return s.createSubject("CSF103", "Fundamentals of Programming").then(()=>{
        return s.createSubject("CSF104", "Modern Database Design").then(()=>{
        return s.createSubject("CSF105", "Back End Development").then(()=>{
        return s.createSubject("CSF106", "Mathematics for Programming I").then(()=>{
        return s.createSubject("CSF107", "Data Structures and Algorithms").then(()=>{
        return s.subjectsCount().then(async(count)=>{
        assert.equal(count,6)
        })
        })
        })
        })
        })
        })
        })
        })
        })
        it('test marks adding', async () => {
            return MarksList.deployed().then((instance) => {
            m = instance;
            return m.addMarks(2, "CSF101",1).then(async (transaction)=> {
            return m.marksCount().then(async(count)=>{
            assert.equal(count,1)
            return m.findMarks(2, "CSF101").then(async(marks)=>{
            assert.equal(marks.code,"CSF101")
            assert.equal(marks.grades, 1 )
            })
            })
            })
            })
            })
            })
            function isValidAddress(address){
            assert.notEqual(address, 0x0)
            assert.notEqual(address, '')
            assert.notEqual(address, null)
            assert.notEqual(address, undefined)
            }
            