// const SubjectList = artifacts.require('SubjectList')

// contract('SubjectList',(account)=>{
//     beforeEach(async ()=>{
//         this.subjectList = await SubjectList.deployed()
//     })

//     it('deploys successfully', async () => {    // it is also function mocha testy framework 
//         // and it used to define the new test case
//         const address = await this.subjectList.address
//         isValidAddress(address)
//     })
  
//     it('test adding subject', async () => {
//         return SubjectList.deployed().then ((instance) => {
//             s = instance;
//             code = "csb203";
//             return s.createSubject(code, "dApps");
//         }).then((transcation) => {
//             // console.log("transcation"+transcation)
//             isValidAddress(transcation.tx)
//             isValidAddress(transcation.receipt.blockHash);
//             return s.subjectsCount()
//         }).then((count) => {
//             assert.equal(count, 1)
//             return s.subjects(1);
//         }).then((subject) => {
//             assert.equal(subject.code, code)
//         })
//     })
//     it('test finding subject', async() =>{
//         return SubjectList.deployed().then(async(instance) => {
//             s = instance;
//             code = 2;
//             s.createSubject(code ++, "python");
//             s.createSubject(code ++ , "Dzongkha");
//             return s.subjectsCount().then(async (count) => {
//                 assert.equal(count, 3)
//             return s.findSubject(2).then(async(subjects) =>{
//                 assert.equal(subjects.subject, "python")
//                 // console.log(subjects)
//             })    
//             })
//         })
//     })
//     it('test mark retired subject', async() =>{
//         return SubjectList.deployed().then(async (instance) =>{
//             s= instance;
//             return s.findSubject(2).then(async(osubject) => {
//                 assert.equal(osubject.subject, "python")
//                 assert.equal(osubject.retired, false)
//                 return s.markRetired(2).then(async(transcation) =>{
//                     return s.findSubject(2).then(async (nsubject) =>{
//                         assert.equal(nsubject.subject, "python")
//                         assert.equal(nsubject.retired, true)
//                     })
//                 })
//             })
//         })
//     })
//     it('test the updating of subject', async() =>{
//         return SubjectList.deployed().then(async(instance) =>{
//             s = instance;
//             s.createSubject("IT101","programming");
//             s.updatedSubject(4,"IT102","programming langauge");
//             return s.findSubject(4).then(async (nsubject) => {
//                 assert.equal(nsubject.code,"IT102",)
//                 assert.equal(nsubject.subject,"programming langauge")
//             })
               
//         })
//     })
// })
// function isValidAddress(address){
//     assert.notEqual(address, 0x0) // assert.notEqual is also function that takes parameter acutal and expected values
//     assert.notEqual(address,'')
//     assert.notEqual(address,null)
//     assert.notEqual(address,undefined)
// }


//Import the SubjectList smart contract
const SubjectList = artifacts.require('SubjectList')
//Use the contract to write all tests
//variable: account => all accounts in blockchain
contract('SubjectList', (accounts) => {
    beforeEach(async () => {
    this.subjectList = await SubjectList.deployed()
    })
//Testing the deployed subject contract
it('deploys successfully', async () => {
//Get the address which the subject object is stored
const address = await this.subjectList.address
//Test for valid address
isValidAddress(address)
})
//Testing the content in the contract
it('test adding subjects', async () => {
return SubjectList.deployed().then(async (instance) => {
s = instance;
subjectcode = "CSF101";
return s.createSubject(subjectcode, "Front End Development")
.then(async (transaction) => {
isValidAddress(transaction.tx)
isValidAddress(transaction.receipt.blockHash);
return s.subjectsCount().then(async (count) => {
assert.equal(count, 1)
return s.subjects(1).then(async (subject) => {
assert.equal(subject.code, subjectcode)
return
})
})
})
})
})
it('test finding subjects', async () => {
return SubjectList.deployed().then((instance) => {
s = instance;
return s.createSubject("CSF102", "Fundamentals of Computing").then(() => {
return s.createSubject("CSF103", "Fundamentals of Programming").then(() => {
return s.createSubject("CSF104", "Modern Database Design").then(() => {
    return s.createSubject("CSF105", "Back End Development").then(() => {
        return s.createSubject("CSF106", "Mathematics for Programming I").then(() =>
        {
        return s.createSubject("CSF107", "Data Structures and Algorithms").then(() => {
        return s.subjectsCount().then(async (count) => {
        assert.equal(count, 7)
        return s.findSubject(1).then((subject) => {
        assert.equal(subject.name, "Front End Development")
        })
        })
        })
        })
        })
        })
        })
        })
        })
        })
        it('test mark retired subjects', async () => {
        return SubjectList.deployed().then((instance) => {
        s = instance;
        return s.findSubject(1).then((subject) => {
        assert.equal(subject.name, "Front End Development")
        assert.equal(subject.retired, false)
        return s.markRetired(1).then((subject) => {
        return s.findSubject(1).then((subject) => {
        assert.equal(subject.name, "Front End Development")
        assert.equal(subject.retired, true)
        })
        })
        })
        })
        })
        })
        function isValidAddress(address) {
        assert.notEqual(address, 0x0)
        assert.notEqual(address, '')
        assert.notEqual(address, null)
        assert.notEqual(address, undefined)
        }
        
