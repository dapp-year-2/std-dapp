module.exports = {
  networks: {
  development: {
  host: "host.docker.internal", // Localhost (default: none)
  port: 7545, // Standard Ethereum port (default: none)
  network_id: "5777", // Any network (default: none)
  },
  },
  // Set default mocha options here, use special reporters etc.
  mocha: {
  // timeout: 100000
  },
  // Configure your compilers
  compilers: {
    solc: {
      version: "0.8.0", // Fetch exact version from solc-bin
      
      optimizer: {
      enabled: true,
      runs: 200
      },
      }
      },
      // Truffle DB is currently disabled by default; to enable it,
     
     
      }
      ;
      
  